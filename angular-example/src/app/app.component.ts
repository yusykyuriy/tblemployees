import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './shared/core/service/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-example';

  constructor(
      private service: UserService,
      private _cookieService: CookieService,
      public router: Router,
  ) {

  }

  goToLogout(){
    this._cookieService.delete('token');
    this.service.logout().subscribe((data:any)=>{});
  }
}
