import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
//SERVICES
import { EmployeeService } from '../shared/core/service/employees.service';
import { DepartmentService } from '../shared/core/service/departments.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  p: number = 1;
  collection: any = [];
  currentPage: number = 1;
  itemsPerPage: number = 2;
  selectedEmployee: object = {};
  arrDepartments:any = [];
  searchText: string;

  modalOptions: NgbModalOptions;
  closeResult: string;
  constructor(
      private employeeService: EmployeeService,
      private departmentService: DepartmentService,
      private modalService: NgbModal
  ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    };
    this.getEmployees();
  }

  async getEmployees() {
    this.arrDepartments = [];
    let departments:any = await this.departmentService.getListPromise();
    for(let department of departments) {
      this.arrDepartments.push({label: department.dpName, value: department.dpId});
    }
    let employees: any = await this.employeeService.getListPromise();
    this.collection = employees;
  }

  async removeEmployee(id) {
    if(await this.employeeService.deleteEmployeePromise(id)) {
      this.getEmployees();
    }
  }

  open(content, item) {
    this.selectedEmployee = item;
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  async saveEmployee() {
    debugger;
    if(await this.employeeService.updateEmployeePromise(this.selectedEmployee)) {
      this.getEmployees();
    };
  }

  ngOnInit() { }

}
