import { NgModule, Optional, SkipSelf }  from '@angular/core';
import { CommonModule }                  from '@angular/common';

//SERVICES
import { EmployeeService }   from './service/employees.service';
import { DepartmentService } from './service/departments.service';
import { UserService }       from './service/users.service';

@NgModule({
    imports: [ CommonModule ],
    declarations: [],
    exports: [],
    providers: [
        EmployeeService,
        DepartmentService,
        UserService
    ]
})

export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule:CoreModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}