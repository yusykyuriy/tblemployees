import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../constants';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class DepartmentService {
    constructor(
        public http: HttpClient,
        private _cookieService: CookieService
    ) {}

    getList() {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8',
            'token': this._cookieService.get('token')
        });
        return this.http.request('GET', Constants.hostName + 'api/departments', {observe: 'response', headers: headers});
    }

    getListPromise() {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8',
                'token': this._cookieService.get('token')
            });
            this.http.request('GET', Constants.hostName + 'api/departments', {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body)
            })
        })
    }

}