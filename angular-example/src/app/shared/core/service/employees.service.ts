import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../constants';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class EmployeeService {
    constructor(
        public http: HttpClient,
        private _cookieService: CookieService
    ) {}

    getList() {
        let headers = new HttpHeaders({
            'Content-Type': 'application/json;charset=utf-8',
            'token': this._cookieService.get('token')
        });
        return this.http.request('GET', Constants.hostName + 'api/employees', {observe: 'response', headers: headers});
    }

    getListPromise() {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8',
                'token': this._cookieService.get('token')
            });
            this.http.request('GET', Constants.hostName + 'api/employees', {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body)
            })
        })
    }


    addEmployee(body: any) {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8',
                'token': this._cookieService.get('token')
            });
            this.http.post(Constants.hostName + 'api/employees', body, {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body)
            })
        })
    }

    updateEmployeePromise(body: any) {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8',
                'token': this._cookieService.get('token')
            });
            this.http.put(Constants.hostName + 'api/employees', body, {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body)
            })
        })
    }

    deleteEmployeePromise(id: any) {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json;charset=utf-8',
                'token': this._cookieService.get('token')
            });
            this.http.delete(Constants.hostName + 'api/employees/' + id, {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body)
            })
        })
    }
}