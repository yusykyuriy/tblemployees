import {Injectable}                 from '@angular/core';
import {HttpClient, HttpHeaders}    from '@angular/common/http';
import {Constants}                  from '../constants';
import { CookieService }            from 'ngx-cookie-service';

@Injectable()
export class UserService {
    constructor(
        public http: HttpClient,
        private _cookieService: CookieService
    ) {
    }

    login(body: any) {
        let headers = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});
        return this.http.post(Constants.hostName + 'api/user/login', body, {observe: 'response', headers: headers});
    }

    checkToken(token: any) {
        let headers = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});
        return this.http.post(Constants.hostName + 'api/user/checktoken', {token: token}, {
            observe: 'response',
            headers: headers
        });
    }

    checkTokenPromise(token: any) {
        return new Promise((resolve) => {
            let headers = new HttpHeaders({'Content-Type': 'application/json;charset=utf-8'});
            this.http.post(Constants.hostName + 'api/user/checktoken', {token: token}, {
                observe: 'response',
                headers: headers
            }).subscribe((data: any) => {
                resolve(data.body.next)
            })
        })
    }

    logout() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', 'token': this._cookieService.get('token')});
        return this.http.request('GET', Constants.hostName + 'api/user/logout', {observe: 'response', headers: headers});
    }
}
