import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Router} from '@angular/router';
import 'rxjs/Rx';
//SERVICES
import {CookieService} from 'ngx-cookie-service';
import {UserService} from '../service/users.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private service: UserService,
        private _cookieService: CookieService
    ) {}

    async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this._cookieService.get('token')) {
            let next: any = await this.service.checkTokenPromise(this._cookieService.get('token'));
            return next;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
