import { Component, OnInit } from '@angular/core';
import { CookieService }     from 'ngx-cookie-service';
import { Router }            from '@angular/router';

import { UserService }       from '../shared/core/service/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser: object = {
    username:'',
    password:''
  };

  constructor(
      public router: Router,
      private _cookieService: CookieService,
      private service: UserService
  ) { }


  async login(){
    debugger;
    this.service.login(this.loginUser).subscribe((data:any)=>{
      if(data.body&&data.body.token){
        let expire = 0.9;
        debugger;
        this._cookieService.delete('token');
        this._cookieService.set('token', data.body.token, expire);
        this.router.navigate(['/']);
      }
    }, async (err)=>{
      alert('Error, incorrect username or password');
    });
  }

  ngOnInit() {
  }

}
