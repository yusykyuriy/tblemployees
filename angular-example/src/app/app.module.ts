import { BrowserModule }                    from '@angular/platform-browser';
import { NgModule }                         from '@angular/core';
import { HttpClientModule }                 from '@angular/common/http';
import { NgxPaginationModule }              from 'ngx-pagination';
import { NgbModule }                        from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule }                       from './shared/core/core.module';

import { AppRoutingModule }                 from './app-routing.module';
import { AppComponent }                     from './app.component';
import { RegisterComponent }                from './register/register.component';
import { HomeComponent }                    from './home/home.component';
import { LoginComponent }                   from './login/login.component';
import { EmployeesComponent }               from './employees/employees.component';

//SERVICES
import { CookieService }                    from 'ngx-cookie-service';
import { AuthGuard }                        from './shared/core/guard/auth.guard';

import { FilterPipe }                       from './filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    LoginComponent,
    EmployeesComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    CookieService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
