var jwt = require('jsonwebtoken');
var config = require('../config/environment');


exports.getToken = function (user) {
    return jwt.sign({
        _doc: {
            _id: user._id,
            username: user.username
        }
    }, config.secretKey, {
        expiresIn: 1000
    });
};

exports.verifyAdmin = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['token'];
    // decode token
    if (token) {
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (!decoded._doc.username) {
                var err2 = new Error('You are not authorized to perform this operation!');
                err2.status = 403;
                return next(err2);
            }
            return next();
        });
    } else {
        var err = new Error('No token provided!');
        err.status = 403;
        return next(err);
    }

};



