var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('../db');

exports.local = passport.use('local', new LocalStrategy({
        username: 'admin',
        password: 'Qwerty1!!!',
        passReqToCallback: true
    },
    function (req, username, password, done) {
        db.query("SELECT * FROM `tbl_users` WHERE `username` = '" + username + "'", function (err, rows) {
            if (err)
                return done(err);
            if (!rows.length) {
                return done(null, false, 'No');
            }

            if (!( rows[0].password == password))
                return done(null, false, 'No');

            return done(null, rows[0]);
        });
    }));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    db.query("select * from tbl_users where id = " + id, function (err, rows) {
        done(err, rows[0]);
    });
});
