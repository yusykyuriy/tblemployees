'use strict';
// Development specific configuration
// ==================================
module.exports = {
    ip: process.env.OPENSHIFT_NODEJS_IP || process.env.IP || undefined,// Server IP
    port: process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080,// Server port
    // MongoDB connection options
    mysql: {uri: '127.0.0.1'},
    httpAuth: {
        username: 'admin',
        password: 'Qwerty1!!!'
    },
    secretKey: '12345-67890-09876-54321'//for token
};
