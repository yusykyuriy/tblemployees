'use strict';

var path = require('path');
var _ = require('lodash');
var crypto = require('crypto');

function requiredProcessEnv(name) {
    if (!process.env[name]) { throw new Error('You must set the ' + name + ' environment variable'); }
  return process.env[name];
}

var all = {
    env: process.env.NODE_ENV,
    root: path.normalize(__dirname + '/../..'),// Root path of server
    port: process.env.PORT || 8000,// Server port
    ip: process.env.IP || '0.0.0.0',// Server IP
    secrets: { session: 'df8d897dvhf987897' },// Secret for session, you will want to change this and make it an environment variable//session: 'fullstack-demo-secret'
    userRoles: ['guest', 'user', 'admin', 'owner'],// List of user roles
    secretKey: '12345-67890-09876-54321'//for token
};

function hdSuc(res, val) { return res.connection ? res.status(200).json(all.encodeStr(JSON.stringify(val))) : val; }

module.exports = _.merge(all, require('./' + process.env.NODE_ENV + '.js') || {});
