/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var path = require('path');

module.exports = function (app) {
    // Insert routes below
    //app.use('/auth', require('./auth'));
    app.use('/api/token', require('./api/token'));
    app.use('/api/user', require('./api/user'));
    app.use('/api/employees', require('./api/employee'));
    app.use('/api/departments', require('./api/department'));
    app.use('/api/user', require('./api/user'));

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth)/*')
     .get(errors[404]);

    // All other routes should redirect to the index.html
    app.route('/*')
      .get(function (req, res) {
          res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
      });
};
