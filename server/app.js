'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var config = require('./config/environment');

const express = require("express");
var cors = require('cors');
var authenticate = require('./auth/authenticate');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const passport = require('passport');
const passportJWT = require('passport-jwt');

var ExtractJwt = passportJWT.ExtractJwt;
var  JwtStrategy = passportJWT.Strategy;

var jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = 'wowwow';

// lets create our strategy for web token
var strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    console.log('payload received', jwt_payload);
    var user = getUser({ id: jwt_payload.id });

    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
});

passport.use(strategy);

const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: false});

app.options('http://localhost:4200', cors());
app.options('http://localhost:8100', cors());
app.use(cors({ credentials: true, "origin": "http://localhost:4200" }, { credentials: true, "origin": "http://localhost:8100" }));

var server = require('http').createServer(app);
require('./config/express')(app);
require('./routes')(app);
app.listen(config.port, function() { console.log('Express server listening on %d, in %s mode', config.port, app.get('env')); });
app.timeout = 30000000;
exports = module.exports = app;
