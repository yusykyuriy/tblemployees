'use strict';

var fetch = require('node-fetch');
var db = require('../../db');

function handleError(res, err) { return res.connection ? res.status(500).send(err) : err; }
function handleSuccess(res, val) { return res.connection ? res.status(200).json(val) : val; }

exports.list = function (req, res) {
    db.query('SELECT * FROM tbl_departments', function(err, result) {
        if (err) return handleError(res, err);
        handleSuccess(res, result);
    });
};


