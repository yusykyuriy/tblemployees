'use strict';
var express = require('express');
var controller = require('./department.controller');
var router = express.Router();
var Verify = require('../../auth/verify');

router.get('/', controller.list);

module.exports = router;