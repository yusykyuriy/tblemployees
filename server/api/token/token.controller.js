'use strict';
var _ = require('lodash');
var db = require('../../db');
var moment = require('moment');
var config = require('../../config/environment');

function handleError(res, err) {
    return res.connection ? res.status(500).send(err) : err;
}
function handleSuccess(res, val) {
    return res.connection ? res.status(200).json(val) : val;
}

exports.token = function (req, res) {
    db.query("SELECT * FROM tbl_tokens WHERE key=?", ['a1'], function (err, obj) {
        if (err) return handleError(res, err);
        var existstoken = 0;
        if (obj) {
            var expiresDate = Date.parse(obj.expires);
            var now = +new Date();
            if (expiresDate && expiresDate > now) {
                existstoken = 2;
            } else {
                existstoken = 1;
            }
        }
        if (existstoken > 1) {
            handleSuccess(res, obj);
        } else {
            var obj = {
                "key": "a1",
                "access_token": "-cxtWiUsrRiQMV7I7QT-1D5-b7qEz4UIcXhB6fsPAp-cO3mwXqzxqoULDEgNOWtN66pgvQUn6_agsrUqruvuP6G3mWuf6fQm3aZ1gQyNXHneXkX3S4xWig9fk8KRbo9XdCIWYHjl-w3IGmjH9MLT92y3vmnLd0g3Q-wLdAsi_FfCvTjJl9bsJZqkI3w1j8BFnDSUykUKD70_pQabWihq7DmkLUUgy2mdH2rrWed1twhnHTooJVwCBnfrlFewBRoxMa73ArqPkuQC45TjvYwDn5I1aTZHYOH-6keRgCN_Fvz1EdJ_iFzZJu0-3FO_TiIwETJk19_J-De18TnE6NtU7xbIbBQ9Xak13Ob8MQ",
                "token_type": "bearer",
                "expires_in": 300,
                "refresh_token": "ecc3c363-fc84-4072-95d1-828b65fdafac",
                "_id": "1b452bc7-0f8f-43c2-9367-486fe898003d",
                "issued": "Wed, 15 May 2019 14:33:34 GMT",
                "expires": "2019-05-17T02:33:34"
            };

            if (existstoken) {
                db.query("SELECT * FROM tbl_tokens WHERE key=?", ['a1'], function (err, obj) {
                    if (err || !obj) return handleError(res, {err: 'error create token'});
                    var updated = _.extend(obj, data);
                    updated.save(function (err, obj) {
                        handleSuccess(res, obj);
                    });
                });
            } else {
                db.query("INSERT INTO tbl_tokens (key, refresh_token, _id, issued, expires, access_token, token_type, expires_in) VALUES (?,?,?,?,?,?,?,?)", [obj.key, obj.refresh_token, obj._id, obj.issued, obj.expires, obj.access_token, obj.token_type, obj.expires_in], function (err, obj) {
                    if (err) return handleError(res, err);
                    handleSuccess(res, obj);
                });
            }
        }
    });
};
