'use strict';
var express = require('express');
var controller = require('./employee.controller');
var router = express.Router();
var Verify = require('../../auth/verify');

router.get('/', Verify.verifyAdmin, controller.list);
router.put('/', Verify.verifyAdmin, controller.update);
router.delete('/:id', Verify.verifyAdmin, controller.remove);
module.exports = router;