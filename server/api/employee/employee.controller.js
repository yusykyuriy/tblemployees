'use strict';

var fetch = require('node-fetch');
var db = require('../../db');

function handleError(res, err) { return res.connection ? res.status(500).send(err) : err; }
function handleSuccess(res, val) { return res.connection ? res.status(200).json(val) : val; }

exports.list = function (req, res) {
    db.query('SELECT * FROM tbl_employees JOIN tbl_departments ON tbl_employees.emp_dpID = tbl_departments.dpId', function(err, result) {
        if (err) return handleError(res, err);
        handleSuccess(res, result);
    });
};

// Updates an existing
exports.update = function (req, res) {
    if(!req.body) return res.sendStatus(400);
    const empId = req.body.empId;
    const empName = req.body.empName;
    const empActive = req.body.empActive;
    const emp_dpId = req.body.emp_dpID;

    db.query("UPDATE tbl_employees SET empName=?, empActive=?, emp_dpId=? WHERE empId=?", [empName, empActive, emp_dpId, empId], function(err, result) {
        if (err) return handleError(res, err);
        handleSuccess(res, result);
    });
};

exports.remove = function (req, res) {
    const id = req.params.id;
    if(id){
        db.query("DELETE FROM tbl_employees WHERE empId=?", [id], function(err, result) {
            if (err) return handleError(res, err);
            handleSuccess(res, result);
        });
    }
};

