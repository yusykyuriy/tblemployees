'use strict';
var passport = require('passport');
var Verify = require('../../auth/verify');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');

exports.login = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(401).json({
                err: info
            });
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }

            if (!user.username) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }

            var token = Verify.getToken(user);
            res.status(200).json({
                status: 'Login successful!',
                success: true,
                token: token
            });
        });
    })(req, res, next);
};
exports.checkToken = function (req, res) {
    var token = req.body.token || req.query.token || req.headers['token'];
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, config.secretKey, function (err, decoded) {
            if (!decoded || !decoded._doc || !decoded._doc.username) {
                res.status(200).json({
                    next: false
                });
            } else {
                res.status(200).json({
                    next: true
                });
            }
        });
    } else {
        res.status(200).json({
            next: false
        });
    }
};

exports.logout = function (req, res) {
    req.logout();
    res.status(200).json({
        status: 'Bye!'
    });
};

/*
 exports.register = function (req, res, next) { for to create user
 User.register(new User(
 {username: req.body.username}),
 req.body.password, function (err, user) {
 console.log(req.body);
 if (err) {
 return res.status(500).json({err: err});
 }
 user.save(function (err, user) {
 passport.authenticate('local')(req, res, function () {
 return res.status(200).json({status: 'Registration Successful!'});
 });
 });
 });
 };*/
