const mysql = require("mysql2");
const connection = mysql.createPool({
    host: "127.0.0.1",
    user: "root",
    password: "",
    database: "employees",
    port: 3306
});


module.exports = connection;